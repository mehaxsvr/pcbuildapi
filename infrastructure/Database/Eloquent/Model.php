<?php

namespace Infrastructure\Database\Eloquent;

use Illuminate\Database\Eloquent\Model as BaseModel;

/**
 * Class Model
 * @package Infrastructure\Database\Eloquent
 * @property-read int id
 * @property-read string created_at
 * @property-read string updated_at
 */
abstract class Model extends BaseModel
{
}
