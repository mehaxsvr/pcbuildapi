<?php


namespace Infrastructure\Database\Traits;

use Illuminate\Support\Collection;
use Infrastructure\Database\Eloquent\Model;

/**
 * Trait EditableTrait
 * @package Infrastructure\Database\Traits
 */
trait EditableTrait
{
    /**
     * @param Model|Collection $model
     * @param array $data
     * @return Model
     */
    public function update($model, array $data)
    {
        $model->fill($data);
        $model->save();
        return $model;
    }
}