<?php


namespace Infrastructure\Database\Traits;

use Infrastructure\Database\Eloquent\Model;

/**
 * Trait Creatable
 * @package Infrastructure\Database\Traits
 * @method Model getModel()
 */
trait CreatableTrait
{
    /**
     * @param array $data
     * @return Model
     */
    public function create(array $data)
    {
        $model = $this->getModel();
        $model->fill($data);
        $model->save();
        return $model;
    }
}