<?php

namespace Api\Users\Repositories;

use Api\Users\Models\User;
use Illuminate\Support\Collection;
use Infrastructure\Database\Eloquent\Repository;

class UserRepository extends Repository
{
    public function getModel()
    {
        return new User();
    }

    public function create(array $data)
    {
        $user = $this->getModel();

        $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);

        $user->fill($data);
        $user->save();

        return $user;
    }

    /**
     * @param User|Collection $user
     * @param array $data
     * @return User
     */
    public function update(User $user, array $data)
    {
        $user->fill($data);

        $user->save();

        return $user;
    }
}
