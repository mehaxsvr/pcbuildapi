<?php


namespace Api\Fields\Exceptions;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CaptionNotFoundException extends NotFoundHttpException
{
    public function __construct($id)
    {
        parent::__construct('Caption not found: '.$id);
    }
}