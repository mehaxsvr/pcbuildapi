<?php


namespace Api\Fields\Exceptions;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FieldNotFoundException extends NotFoundHttpException
{
    public function __construct($id)
    {
        parent::__construct('Field not found: '.$id);
    }
}