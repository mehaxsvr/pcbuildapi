<?php


namespace Api\Fields\Exceptions;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TypeFieldNotFoundException extends NotFoundHttpException
{
    public function __construct($id)
    {
        parent::__construct('TypeField not found: '.$id);
    }
}
