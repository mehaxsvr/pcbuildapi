<?php

/** @var \Illuminate\Routing\Router $router */
$router->get('/fields', 'FieldsController@getAll');
$router->get('/fields/{id}', 'FieldsController@getById');
$router->post('/fields', 'FieldsController@create');
$router->put('/fields/{id}', 'FieldsController@update');
$router->delete('/fields/{id}', 'FieldsController@delete');

$router->get('/captions', 'CaptionsController@getAll');
$router->get('/captions/{id}', 'CaptionsController@getById');
$router->post('/captions', 'CaptionsController@create');
$router->put('/captions/{id}', 'CaptionsController@update');
$router->delete('/captions/{id}', 'CaptionsController@delete');

$router->get('/type-fields', 'TypeFieldController@getAll');
$router->get('/type-fields/{id}', 'TypeFieldController@getById');
$router->post('/type-fields', 'TypeFieldController@create');
$router->put('/type-fields/{id}', 'TypeFieldController@update');
$router->delete('/type-fields/{id}', 'TypeFieldController@delete');
