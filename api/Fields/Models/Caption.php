<?php


namespace Api\Fields\Models;


use Api\Components\Models\Type;
use Infrastructure\Database\Eloquent\Model;

/**
 * Class Caption
 * @package Api\Fields\Models
 * @property boolean is_main
 * @property string value
 * @property integer parent_id
 * @property integer type_id
 * @property integer field_id
 *
 * @property-read Caption parent
 * @property-read Type type
 * @property-read Field field
 */
class Caption extends Model
{
    protected $table = 'captions';

    protected $fillable = [
        'is_main', 'value',
        'parent_id', 'type_id', 'field_id',
    ];

    protected $casts = [
        'is_main' => 'boolean',
    ];

    public function parent()
    {
        return $this->belongsTo(Caption::class, 'parent_id');
    }

    public function type()
    {
        return $this->belongsTo(Type::class, 'type_id');
    }

    public function field()
    {
        return $this->belongsTo(Field::class, 'field_id');
    }
}
