<?php


namespace Api\Fields\Models;


use Infrastructure\Database\Eloquent\Model;

/**
 * Class Field
 * @package Api\Fields\Models
 * @property string name
 */
class Field extends Model
{
    protected $table = 'fields';

    protected $fillable = [
        'name'
    ];
}