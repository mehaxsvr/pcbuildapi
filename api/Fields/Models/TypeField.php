<?php


namespace Api\Fields\Models;


use Api\Components\Models\Type;
use Infrastructure\Database\Eloquent\Model;

/**
 * Class TypeField
 * @package Api\Fields\Models
 * @property int type_id
 * @property int field_id
 * @property-read string created_at
 * @property-read string updated_at
 *
 * @property-read Type type
 * @property-read Field field
 */
class TypeField extends Model
{
    protected $table = 'type_field';

    protected $fillable = [
        'type_id', 'field_id',
    ];

    public function type()
    {
        return $this->belongsTo(Type::class, 'type_id');
    }

    public function field()
    {
        return $this->belongsTo(Field::class, 'field_id');
    }
}
