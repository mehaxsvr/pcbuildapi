<?php


namespace Api\Fields\Repositories;


use Api\Fields\Models\TypeField;
use Infrastructure\Database\Eloquent\Repository;
use Infrastructure\Database\Traits\CreatableTrait;
use Infrastructure\Database\Traits\EditableTrait;

class TypeFieldRepository extends Repository
{
    use CreatableTrait, EditableTrait;

    protected function getModel()
    {
        return new TypeField();
    }
}
