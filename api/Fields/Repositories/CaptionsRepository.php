<?php


namespace Api\Fields\Repositories;


use Api\Fields\Models\Caption;
use Infrastructure\Database\Eloquent\Repository;
use Infrastructure\Database\Traits\CreatableTrait;
use Infrastructure\Database\Traits\EditableTrait;

class CaptionsRepository extends Repository
{
    use CreatableTrait, EditableTrait;

    protected function getModel()
    {
        return new Caption();
    }
}