<?php


namespace Api\Fields\Repositories;


use Api\Fields\Models\Field;
use Infrastructure\Database\Eloquent\Repository;
use Infrastructure\Database\Traits\CreatableTrait;
use Infrastructure\Database\Traits\EditableTrait;

class FieldsRepository extends Repository
{
    use CreatableTrait, EditableTrait;

    protected function getModel()
    {
        return new Field();
    }
}