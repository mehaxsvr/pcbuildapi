<?php


namespace Api\Fields\Controllers;


use Api\Fields\Services\CaptionsService;
use Illuminate\Http\Request;
use Infrastructure\Api\Controllers\DefaultApiController;

class CaptionsController extends DefaultApiController
{
    /**
     * @var CaptionsService
     */
    private $captionsService;

    public function __construct(CaptionsService $captionsService)
    {
        $this->captionsService = $captionsService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->captionsService->getAll();
        $parsedData = $this->parseData($data, $resourceOptions, 'data');

        return $this->response($parsedData);
    }

    public function getById($id)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->captionsService->getById($id);
        $parsedData = $this->parseData($data, $resourceOptions, 'data');

        return $this->response($parsedData);
    }

    public function create(Request $request)
    {
        $data = $request->get('data');

        $caption = $this->captionsService->create($data);

        return $this->response([
            'status' => true,
            'message' => 'The caption has been created',
            'data' => $caption,
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->get('data');

        $caption = $this->captionsService->updateById($id, $data);

        return $this->response([
            'status' => true,
            'message' => 'The caption has been created',
            'data' => $caption,
        ]);
    }

    public function delete($id)
    {
        $this->captionsService->deleteById($id);

        return $this->response([
            'status' => true,
            'message' => 'The caption has been deleted',
        ]);
    }
}
