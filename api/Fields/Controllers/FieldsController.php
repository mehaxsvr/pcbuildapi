<?php


namespace Api\Fields\Controllers;


use Api\Fields\Services\FieldsService;
use Illuminate\Http\Request;
use Infrastructure\Api\Controllers\DefaultApiController;

class FieldsController extends DefaultApiController
{
    /**
     * @var FieldsService
     */
    private $fieldsService;

    public function __construct(FieldsService $fieldsService)
    {
        $this->fieldsService = $fieldsService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->fieldsService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'data');

        return $this->response($parsedData);
    }

    public function getById($id)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->fieldsService->getById($id);
        $parsedData = $this->parseData($data, $resourceOptions, 'data');

        return $this->response($parsedData);
    }

    public function create(Request $request)
    {
        $data = $request->get('data');

        $field = $this->fieldsService->create($data);

        return $this->response([
            'status' => true,
            'message' => 'The field has been created',
            'data' => $field,
        ]);
    }

    public function update($id, Request $request)
    {
        $data = $request->get('data');

        $field = $this->fieldsService->updateById($id, $data);

        return $this->response([
            'status' => true,
            'message' => 'The field has been updated',
            'data' => $field,
        ]);
    }

    public function delete($id)
    {
        $this->fieldsService->deleteById($id);

        return $this->response([
            'status' => true,
            'message' => 'The field has been deleted'
        ]);
    }
}
