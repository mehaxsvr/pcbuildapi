<?php


namespace Api\Fields\Services;


use Api\Fields\Exceptions\FieldNotFoundException;
use Api\Fields\Exceptions\TypeFieldNotFoundException;
use Api\Fields\Models\Field;
use Api\Fields\Models\TypeField;
use Api\Fields\Repositories\FieldsRepository;
use Api\Fields\Repositories\TypeFieldRepository;
use Illuminate\Database\Eloquent\Collection;
use Infrastructure\Database\Eloquent\Model;

class FieldsService
{
    /**
     * @var FieldsRepository
     */
    private $fieldsRepository;

    /**
     * @var TypeFieldRepository
     */
    private $typeFieldRepository;

    /**
     * FieldsService constructor.
     * @param FieldsRepository $fieldsRepository
     * @param TypeFieldRepository $typeFieldRepository
     */
    public function __construct(
        FieldsRepository $fieldsRepository,
        TypeFieldRepository $typeFieldRepository
    )
    {
        $this->fieldsRepository = $fieldsRepository;
        $this->typeFieldRepository = $typeFieldRepository;
    }

    /**
     * @param array $resourceOptions
     * @return Collection|Field[]
     */
    public function getAll($resourceOptions = [])
    {
        return $this->fieldsRepository->get($resourceOptions);
    }

    /**
     * @param int $id
     * @return Collection|Field
     */
    public function getById($id)
    {
        $model = $this->fieldsRepository->getById($id);

        if (!$model) {
            throw new FieldNotFoundException($id);
        }

        return $model;
    }

    /**
     * @param array $data
     * @return Field|Model
     */
    public function create(array $data)
    {
        return $this->fieldsRepository->create($data);
    }

    /**
     * @param int $id
     * @param array $data
     * @return Model|Field
     */
    public function updateById($id, array $data)
    {
        $field = $this->getById($id);

        return $this->fieldsRepository->update($field, $data);
    }

    /**
     * @param int $id
     */
    public function deleteById($id)
    {
        $this->fieldsRepository->delete($id);
    }

    /**
     * @param array $resourceOptions
     * @return Collection|TypeField[]
     */
    public function getTypeFields($resourceOptions = [])
    {
        return $this->typeFieldRepository->get($resourceOptions);
    }

    /**
     * @param int $id
     * @return Collection|TypeField
     */
    public function getTypeFieldById($id)
    {
        $typeField = $this->typeFieldRepository->getById($id);

        if (!$typeField) {
            throw new TypeFieldNotFoundException($id);
        }

        return $typeField;
    }

    /**
     * @param array $data
     * @return Model|TypeField
     */
    public function createTypeField(array $data)
    {
        return $this->typeFieldRepository->create($data);
    }

    /**
     * @param int $id
     * @param array $data
     * @return Model|TypeField
     */
    public function updateTypeFieldById($id, array $data)
    {
        $typeField = $this->getTypeFieldById($id);

        return $this->typeFieldRepository->update($typeField, $data);
    }

    /**
     * @param int $id
     */
    public function deleteTypeFieldById($id)
    {
        $this->typeFieldRepository->delete($id);
    }
}
