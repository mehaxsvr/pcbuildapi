<?php


namespace Api\Fields\Services;


use Api\Fields\Exceptions\CaptionNotFoundException;
use Api\Fields\Models\Caption;
use Api\Fields\Repositories\CaptionsRepository;
use Illuminate\Database\Eloquent\Collection;
use Infrastructure\Database\Eloquent\Model;

class CaptionsService
{
    /**
     * @var CaptionsRepository
     */
    private $captionsRepository;

    /**
     * CaptionsService constructor.
     * @param CaptionsRepository $captionsRepository
     */
    public function __construct(CaptionsRepository $captionsRepository)
    {
        $this->captionsRepository = $captionsRepository;
    }

    /**
     * @param array $resourceOptions
     * @return Collection|Caption[]
     */
    public function getAll($resourceOptions = [])
    {
        return $this->captionsRepository->get($resourceOptions);
    }

    /**
     * @param int $id
     * @return Collection|Caption
     */
    public function getById($id)
    {
        $model = $this->captionsRepository->getById($id);

        if (!$model) {
            throw new CaptionNotFoundException($id);
        }

        return $model;
    }

    /**
     * @param array $data
     * @return Model|Caption
     */
    public function create(array $data)
    {
        return $this->captionsRepository->create($data);
    }

    /**
     * @param int $id
     * @param array $data
     * @return Model|Caption
     */
    public function updateById($id, array $data)
    {
        $model = $this->getById($id);

        return $this->captionsRepository->update($model, $data);
    }

    /**
     * @param int $id
     */
    public function deleteById($id)
    {
        $this->captionsRepository->delete($id);
    }
}
