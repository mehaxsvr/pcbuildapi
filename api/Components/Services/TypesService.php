<?php


namespace Api\Components\Services;


use Api\Components\Exceptions\TypeNotFoundException;
use Api\Components\Models\Type;
use Api\Components\Repositories\TypesRepository;
use Illuminate\Database\Eloquent\Collection;
use Infrastructure\Database\Eloquent\Model;

class TypesService
{
    /**
     * @var TypesRepository
     */
    private $typesRepository;

    public function __construct(TypesRepository $typesRepository)
    {
        $this->typesRepository = $typesRepository;
    }

    /**
     * @param array $resourceOptions
     * @return Collection|Type[]
     */
    public function getAll(array $resourceOptions = [])
    {
        return $this->typesRepository->get($resourceOptions);
    }

    /**
     * @param int $id
     * @return Collection|Type
     */
    public function getById($id)
    {
        $data = $this->typesRepository->getById($id);

        if (!$data) {
            throw new TypeNotFoundException($id);
        }

        return $data;
    }

    /**
     * @param array $data
     * @return Type|Model
     */
    public function create(array $data)
    {
        return $this->typesRepository->create($data);
    }

    /**
     * @param int $id
     * @param array $data
     * @return Type|Model
     */
    public function updateById($id, array $data)
    {
        $type = $this->getById($id);

        return $this->typesRepository->update($type, $data);
    }

    /**
     * @param int $id
     */
    public function deleteById($id)
    {
        $this->typesRepository->delete($id);
    }
}
