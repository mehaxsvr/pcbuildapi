<?php


namespace Api\Components\Services;


use Api\Components\Exceptions\ComponentNotFoundException;
use Api\Components\Models\Component;
use Api\Components\Repositories\ComponentsRepository;
use Illuminate\Database\Eloquent\Collection;
use Infrastructure\Database\Eloquent\Model;

class ComponentsService
{
    /**
     * @var ComponentsRepository
     */
    private $componentsRepository;

    public function __construct(ComponentsRepository $componentsRepository)
    {
        $this->componentsRepository = $componentsRepository;
    }

    /**
     * @param array $resourceOptions
     * @return Collection|Component[]
     */
    public function getAll(array $resourceOptions = [])
    {
        return $this->componentsRepository->get($resourceOptions);
    }

    /**
     * @param int $id
     * @return Collection|Component
     */
    public function getById($id)
    {
        $data = $this->componentsRepository->getById($id);

        if (!$data) {
            throw new ComponentNotFoundException($id);
        }

        return $data;
    }

    /**
     * @param array $data
     * @return Component|Model
     */
    public function create(array $data)
    {
        return $this->componentsRepository->create($data);
    }

    /**
     * @param int $id
     * @param array $data
     * @return Model|Component
     */
    public function updateById($id, array $data)
    {
        $component = $this->getById($id);

        return $this->componentsRepository->update($component, $data);
    }

    /**
     * @param int $id
     */
    public function deleteById($id)
    {
        $this->componentsRepository->delete($id);
    }
}
