<?php


namespace Api\Components\Repositories;


use Api\Components\Models\Component;
use Infrastructure\Database\Eloquent\Repository;
use Infrastructure\Database\Traits\CreatableTrait;
use Infrastructure\Database\Traits\EditableTrait;

class ComponentsRepository extends Repository
{
    use CreatableTrait, EditableTrait;

    protected function getModel()
    {
        return new Component();
    }
}