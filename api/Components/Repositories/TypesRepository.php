<?php


namespace Api\Components\Repositories;


use Api\Components\Models\Type;
use Infrastructure\Database\Eloquent\Repository;
use Infrastructure\Database\Traits\CreatableTrait;
use Infrastructure\Database\Traits\EditableTrait;

class TypesRepository extends Repository
{
    use CreatableTrait, EditableTrait;

    protected function getModel()
    {
        return new Type();
    }
}