<?php


namespace Api\Components\Exceptions;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ComponentNotFoundException extends NotFoundHttpException
{
    public function __construct($id)
    {
        parent::__construct('Component not found: '. $id);
    }
}