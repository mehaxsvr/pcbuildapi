<?php


namespace Api\Components\Exceptions;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TypeNotFoundException extends NotFoundHttpException
{
    public function __construct($id)
    {
        parent::__construct('Type not found: '. $id);
    }
}