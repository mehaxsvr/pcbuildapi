<?php

/** @var \Illuminate\Routing\Router $router */
$router->get('/types', 'TypesController@getAll');
$router->get('/types/{id}', 'TypesController@getById');
$router->post('/types', 'TypesController@create');
$router->put('/types/{id}', 'TypesController@update');
$router->delete('/types/{id}', 'TypesController@delete');

$router->get('/components', 'ComponentsController@getAll');
$router->get('/components/{id}', 'ComponentsController@getById');
$router->post('/components', 'ComponentsController@create');
$router->put('/components/{id}', 'ComponentsController@update');
$router->delete('/components/{id}', 'ComponentsController@delete');
