<?php


namespace Api\Components\Models;


use Infrastructure\Database\Eloquent\Model;

/**
 * Class Component
 * @package Api\Components\Models
 * @property integer type_id
 * @property string name
 *
 * @property-read Type type
 */
class Component extends Model
{
    protected $table = 'components';

    protected $fillable = [
        'type_id', 'name',
    ];

    public function type()
    {
        return $this->belongsTo(Type::class, 'type_id');
    }
}