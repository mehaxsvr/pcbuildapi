<?php


namespace Api\Components\Models;


use Infrastructure\Database\Eloquent\Model;

/**
 * Class Type
 * @package Api\Components\Models
 * @property string name
 * @property string short
 */
class Type extends Model
{
    protected $table = 'types';

    protected $fillable = [
        'name', 'short',
    ];
}