<?php


namespace Api\Components\Controllers;


use Api\Components\Services\TypesService;
use Illuminate\Http\Request;
use Infrastructure\Api\Controllers\DefaultApiController;

class TypesController extends DefaultApiController
{
    /**
     * @var TypesService
     */
    private $typesService;

    public function __construct(TypesService $typesService)
    {
        $this->typesService = $typesService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->typesService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'data');

        return $this->response($parsedData);
    }

    public function getById($id)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->typesService->getById($id);
        $parsedData = $this->parseData($data, $resourceOptions, 'data');

        return $this->response($parsedData);
    }

    public function create(Request $request)
    {
        $data = $request->get('data');

        $type = $this->typesService->create($data);

        return $this->response([
            'success' => true,
            'data' => $type,
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->get('data');

        $type = $this->typesService->updateById($id, $data);

        return $this->response([
            'success' => true,
            'data' => $type,
        ]);
    }

    public function delete($id)
    {
        $this->typesService->deleteById($id);

        return $this->response([
            'success' => true,
            'message' => 'Type has been successfully deleted',
        ]);
    }
}
