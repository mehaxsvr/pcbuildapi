<?php


namespace Api\Components\Controllers;


use Api\Components\Services\ComponentsService;
use Illuminate\Http\Request;
use Infrastructure\Api\Controllers\DefaultApiController;

class ComponentsController extends DefaultApiController
{
    /**
     * @var ComponentsService
     */
    private $componentsService;

    public function __construct(ComponentsService $componentsService)
    {
        $this->componentsService = $componentsService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->componentsService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'data');

        return $this->response($parsedData);
    }

    public function getById($id)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->componentsService->getById($id);
        $parsedData = $this->parseData($data, $resourceOptions, 'data');

        return $this->response($parsedData);
    }

    public function create(Request $request)
    {
        $data = $request->get('data');

        $component = $this->componentsService->create($data);

        return $this->response([
            'success' => true,
            'message' => 'The component has been created',
            'data' => $component,
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->get('data');

        $component = $this->componentsService->updateById($id, $data);

        return $this->response([
            'success' => true,
            'message' => 'The component has been updated',
            'data' => $component,
        ]);
    }

    public function delete($id)
    {
        $this->componentsService->deleteById($id);

        return $this->response([
            'success' => true,
            'message' => 'The component has been successfully deleted',
        ]);
    }
}
