<?php

/** @var \Illuminate\Routing\Router $router */
$router->get('/specifications', 'SpecificationsController@getAll');
$router->get('/specifications/{id}', 'SpecificationsController@getById');
$router->post('/specifications', 'SpecificationsController@create');
$router->put('/specifications/{id}', 'SpecificationsController@update');
$router->delete('/specifications/{id}', 'SpecificationsController@delete');

$router->get('/products', 'ProductsController@getAll');
$router->get('/products/{id}', 'ProductsController@getById');
$router->post('/products', 'ProductsController@create');
$router->put('/products/{id}', 'ProductsController@update');
$router->delete('/products/{id}', 'ProductsController@delete');
