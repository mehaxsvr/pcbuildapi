<?php


namespace Api\Products\Repositories;


use Api\Products\Models\Product;
use Infrastructure\Database\Eloquent\Repository;
use Infrastructure\Database\Traits\CreatableTrait;
use Infrastructure\Database\Traits\EditableTrait;

class ProductsRepository extends Repository
{
    use CreatableTrait, EditableTrait;

    protected function getModel()
    {
        return new Product();
    }
}
