<?php


namespace Api\Products\Repositories;


use Api\Products\Models\Specification;
use Infrastructure\Database\Eloquent\Repository;
use Infrastructure\Database\Traits\CreatableTrait;
use Infrastructure\Database\Traits\EditableTrait;

/**
 * Class SpecificationsRepository
 * @package Api\Products\Repositories
 */
class SpecificationsRepository extends Repository
{
    use CreatableTrait, EditableTrait;

    /**
     * @return Specification
     */
    protected function getModel()
    {
        return new Specification();
    }
}
