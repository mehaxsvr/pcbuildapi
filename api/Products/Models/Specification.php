<?php


namespace Api\Products\Models;


use Api\Components\Models\Component;
use Api\Fields\Models\Caption;
use Api\Fields\Models\Field;
use Infrastructure\Database\Eloquent\Model;

/**
 * Class Specification
 * @package Api\Products\Models
 * @property int component_id
 * @property int field_id
 * @property int caption_id
 * @property string value
 *
 * @property-read Component component
 * @property-read Field field
 * @property-read Caption caption
 */
class Specification extends Model
{
    protected $table = 'specs';

    protected $fillable = [
        'component_id', 'field_id', 'caption_id', 'value',
    ];

    public function component()
    {
        return $this->belongsTo(Component::class, 'component_id');
    }

    public function field()
    {
        return $this->belongsTo(Field::class, 'field_id');
    }

    public function caption()
    {
        return $this->belongsTo(Caption::class, 'caption_id');
    }
}
