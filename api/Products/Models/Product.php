<?php


namespace Api\Products\Models;


use Api\Components\Models\Component;
use Api\Stores\Models\Currency;
use Api\Stores\Models\Store;
use Infrastructure\Database\Eloquent\Model;

/**
 * Class Product
 * @package Api\Products\Models
 * @property string title
 * @property float price
 * @property int component_id
 * @property int store_id
 * @property int currency_id
 *
 * @property-read Component component
 * @property-read Store store
 * @property-read Currency currency
 */
class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
        'title', 'price',
        'component_id', 'store_id', 'currency_id',
    ];

    public function component()
    {
        return $this->belongsTo(Component::class, 'component_id');
    }

    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }
}
