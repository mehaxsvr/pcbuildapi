<?php


namespace Api\Products\Controllers;


use Api\Products\Services\SpecificationsService;
use Illuminate\Http\Request;
use Infrastructure\Api\Controllers\DefaultApiController;

class SpecificationsController extends DefaultApiController
{
    /**
     * @var SpecificationsService
     */
    private $specificationsService;

    public function __construct(SpecificationsService $specificationsService)
    {
        $this->specificationsService = $specificationsService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->specificationsService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'data');

        return $this->response($parsedData);
    }

    public function getById($id)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->specificationsService->getById($id);
        $parsedData = $this->parseData($data, $resourceOptions, 'data');

        return $this->response($parsedData);
    }

    public function create(Request $request)
    {
        $data = $request->get('data');

        $model = $this->specificationsService->create($data);

        return $this->response([
            'status' => true,
            'message' => 'The specification has been created',
            'data' => $model->toArray(),
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->get('data');

        $model = $this->specificationsService->updateById($id, $data);

        return $this->response([
            'status' => true,
            'message' => 'The specification has been updated',
            'data' => $model->toArray(),
        ]);
    }

    public function delete($id)
    {
        $this->specificationsService->deleteById($id);

        return $this->response([
            'status' => true,
            'message' => 'The specification has been deleted',
        ]);
    }
}
