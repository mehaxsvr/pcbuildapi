<?php


namespace Api\Products\Controllers;


use Api\Products\Services\ProductsService;
use Illuminate\Http\Request;
use Infrastructure\Api\Controllers\DefaultApiController;

class ProductsController extends DefaultApiController
{
    /**
     * @var ProductsService
     */
    private $productsService;

    public function __construct(ProductsService $productsService)
    {
        $this->productsService = $productsService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->productsService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'data');

        return $this->response($parsedData);
    }

    public function getById($id)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->productsService->getById($id);
        $parsedData = $this->parseData($data, $resourceOptions, 'data');

        return $this->response($parsedData);
    }

    public function create(Request $request)
    {
        $data = $request->get('data');

        $model = $this->productsService->create($data);

        return $this->response([
            'status' => true,
            'message' => 'The product has been created',
            'data' => $model->toArray(),
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->get('data');

        $model = $this->productsService->updateById($id, $data);

        return $this->response([
            'status' => true,
            'message' => 'The product has been updated',
            'data' => $model->toArray(),
        ]);
    }

    public function delete($id)
    {
        $this->productsService->deleteById($id);

        return $this->response([
            'status' => true,
            'message' => 'The product has been deleted',
        ]);
    }
}
