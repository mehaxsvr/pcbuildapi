<?php


namespace Api\Products\Services;


use Api\Products\Exceptions\SpecificationNotFoundException;
use Api\Products\Models\Specification;
use Api\Products\Repositories\SpecificationsRepository;
use Illuminate\Database\Eloquent\Collection;
use Infrastructure\Database\Eloquent\Model;

/**
 * Class SpecificationsService
 * @package Api\Products\Services
 */
class SpecificationsService
{
    /**
     * @var SpecificationsRepository
     */
    private $specificationsRepository;

    public function __construct(SpecificationsRepository $specificationsRepository)
    {
        $this->specificationsRepository = $specificationsRepository;
    }

    /**
     * @param array $resourceOptions
     * @return Collection|Specification[]
     */
    public function getAll($resourceOptions = [])
    {
        return $this->specificationsRepository->get($resourceOptions);
    }

    /**
     * @param int $id
     * @return Collection|Specification
     */
    public function getById($id)
    {
        $model = $this->specificationsRepository->getById($id);

        if (!$model) {
            throw new SpecificationNotFoundException($id);
        }

        return $model;
    }

    /**
     * @param array $data
     * @return Model|Specification
     */
    public function create(array $data)
    {
        return $this->specificationsRepository->create($data);
    }

    /**
     * @param int $id
     * @param array $data
     * @return Model|Specification
     */
    public function updateById($id, array $data)
    {
        $model = $this->getById($id);

        return $this->specificationsRepository->update($model, $data);
    }

    /**
     * @param int $id
     */
    public function deleteById($id)
    {
        $this->specificationsRepository->delete($id);
    }
}
