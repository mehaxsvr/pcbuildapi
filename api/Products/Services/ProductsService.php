<?php


namespace Api\Products\Services;


use Api\Products\Exceptions\ProductNotFoundException;
use Api\Products\Models\Product;
use Api\Products\Repositories\ProductsRepository;
use Illuminate\Database\Eloquent\Collection;
use Infrastructure\Database\Eloquent\Model;

class ProductsService
{
    /**
     * @var ProductsRepository
     */
    private $productsRepository;

    public function __construct(ProductsRepository $productsRepository)
    {
        $this->productsRepository = $productsRepository;
    }

    /**
     * @param array $resourceOptions
     * @return Collection|Product[]
     */
    public function getAll($resourceOptions = [])
    {
        return $this->productsRepository->get($resourceOptions);
    }

    /**
     * @param int $id
     * @return Collection|Product
     */
    public function getById($id)
    {
        $model = $this->productsRepository->getById($id);

        if (!$model) {
            throw new ProductNotFoundException($id);
        }

        return $model;
    }

    /**
     * @param array $data
     * @return Model|Product
     */
    public function create(array $data)
    {
        return $this->productsRepository->create($data);
    }

    /**
     * @param int $id
     * @param array $data
     * @return Model|Product
     */
    public function updateById($id, array $data)
    {
        $model = $this->getById($id);

        return $this->productsRepository->update($model, $data);
    }

    /**
     * @param int $id
     */
    public function deleteById($id)
    {
        $this->productsRepository->delete($id);
    }
}
