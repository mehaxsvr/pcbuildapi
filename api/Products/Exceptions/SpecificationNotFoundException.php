<?php


namespace Api\Products\Exceptions;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SpecificationNotFoundException extends NotFoundHttpException
{
    /**
     * SpecificationNotFoundException constructor.
     * @param int $id
     */
    public function __construct($id)
    {
        parent::__construct("Specification not found: $id");
    }
}
