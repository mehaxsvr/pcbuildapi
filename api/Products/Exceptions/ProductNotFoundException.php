<?php


namespace Api\Products\Exceptions;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductNotFoundException extends NotFoundHttpException
{
    /**
     * SpecificationNotFoundException constructor.
     * @param int $id
     */
    public function __construct($id)
    {
        parent::__construct("Product not found: $id");
    }
}
