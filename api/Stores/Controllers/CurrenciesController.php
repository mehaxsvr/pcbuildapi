<?php


namespace Api\Stores\Controllers;


use Api\Stores\Services\CurrenciesService;
use Illuminate\Http\Request;
use Infrastructure\Api\Controllers\DefaultApiController;

class CurrenciesController extends DefaultApiController
{
    /**
     * @var CurrenciesService
     */
    private $currenciesService;

    public function __construct(CurrenciesService $currenciesService)
    {
        $this->currenciesService = $currenciesService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->currenciesService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'data');

        return $this->response($parsedData);
    }

    public function getById($id)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->currenciesService->getById($id);
        $parsedData = $this->parseData($data, $resourceOptions, 'data');

        return $this->response($parsedData);
    }

    public function create(Request $request)
    {
        $data = $request->get('data');

        $response = $this->currenciesService->create($data);

        return $this->response([
            'status' => true,
            'message' => 'The currency has been created',
            'data' => $response->toArray(),
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->get('data');

        $response = $this->currenciesService->updateById($id, $data);

        return $this->response([
            'status' => true,
            'message' => 'The currency has been updated',
            'data' => $response->toArray(),
        ]);
    }

    public function delete($id)
    {
        $this->currenciesService->deleteById($id);

        return $this->response([
            'status' => true,
            'message' => 'The currency has been deleted',
        ]);
    }
}
