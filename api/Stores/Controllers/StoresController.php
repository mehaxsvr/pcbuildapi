<?php


namespace Api\Stores\Controllers;


use Api\Stores\Services\StoresService;
use Illuminate\Http\Request;
use Infrastructure\Api\Controllers\DefaultApiController;

class StoresController extends DefaultApiController
{
    /**
     * @var StoresService
     */
    private $storesService;

    public function __construct(StoresService $storesService)
    {
        $this->storesService = $storesService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->storesService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'data');

        return $this->response($parsedData);
    }

    public function getById($id)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->storesService->getById($id);
        $parsedData = $this->parseData($data, $resourceOptions, 'data');

        return $this->response($parsedData);
    }

    public function create(Request $request)
    {
        $data = $request->get('data');

        $store = $this->storesService->create($data);

        return $this->response([
            'status' => true,
            'message' => 'The store has been created',
            'data' => $store->toArray(),
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->get('data');

        $store = $this->storesService->updateById($id, $data);

        return $this->response([
            'status' => true,
            'message' => 'The store has been updated',
            'data' => $store->toArray(),
        ]);
    }

    public function delete($id)
    {
        $this->storesService->deleteById($id);

        return $this->response([
            'status' => true,
            'message' => 'The store has been deleted'
        ]);
    }
}
