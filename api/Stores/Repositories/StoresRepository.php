<?php


namespace Api\Stores\Repositories;


use Api\Stores\Models\Store;
use Infrastructure\Database\Eloquent\Repository;
use Infrastructure\Database\Traits\CreatableTrait;
use Infrastructure\Database\Traits\EditableTrait;

/**
 * Class StoresRepository
 * @package Api\Stores\Repositories
 */
class StoresRepository extends Repository
{
    use CreatableTrait, EditableTrait;

    /**
     * @return Store
     */
    protected function getModel()
    {
        return new Store();
    }
}
