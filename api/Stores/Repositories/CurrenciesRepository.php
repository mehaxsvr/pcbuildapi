<?php


namespace Api\Stores\Repositories;


use Api\Stores\Models\Currency;
use Infrastructure\Database\Eloquent\Repository;
use Infrastructure\Database\Traits\CreatableTrait;
use Infrastructure\Database\Traits\EditableTrait;

/**
 * Class CurrenciesRepository
 * @package Api\Stores\Repositories
 */
class CurrenciesRepository extends Repository
{
    use CreatableTrait, EditableTrait;

    /**
     * @return Currency
     */
    protected function getModel()
    {
        return new Currency();
    }
}
