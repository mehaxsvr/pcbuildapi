<?php

/** @var \Illuminate\Routing\Router $router */
$router->get('/stores', 'StoresController@getAll');
$router->get('/stores/{id}', 'StoresController@getById');
$router->post('/stores', 'StoresController@create');
$router->put('/stores/{id}', 'StoresController@update');
$router->delete('/stores/{id}', 'StoresController@delete');

$router->get('/currencies', 'CurrenciesController@getAll');
$router->get('/currencies/{id}', 'CurrenciesController@getById');
$router->post('/currencies', 'CurrenciesController@create');
$router->put('/currencies/{id}', 'CurrenciesController@update');
$router->delete('/currencies/{id}', 'CurrenciesController@delete');
