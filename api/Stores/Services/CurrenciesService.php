<?php


namespace Api\Stores\Services;


use Api\Stores\Exceptions\CurrencyNotFoundException;
use Api\Stores\Models\Currency;
use Api\Stores\Repositories\CurrenciesRepository;
use Illuminate\Database\Eloquent\Collection;
use Infrastructure\Database\Eloquent\Model;

/**
 * Class CurrenciesService
 * @package Api\Stores\Services
 */
class CurrenciesService
{
    /**
     * @var CurrenciesRepository
     */
    private $currenciesRepository;

    /**
     * CurrenciesService constructor.
     * @param CurrenciesRepository $currenciesRepository
     */
    public function __construct(CurrenciesRepository $currenciesRepository)
    {
        $this->currenciesRepository = $currenciesRepository;
    }

    /**
     * @param array $resourceOptions
     * @return Collection|Currency[]
     */
    public function getAll($resourceOptions = [])
    {
        return $this->currenciesRepository->get($resourceOptions);
    }

    /**
     * @param int $id
     * @return Collection|Currency
     */
    public function getById($id)
    {
        $currency = $this->currenciesRepository->getById($id);

        if (!$currency) {
            throw new CurrencyNotFoundException($id);
        }

        return $currency;
    }

    /**
     * @param array $data
     * @return Model|Currency
     */
    public function create(array $data)
    {
        return $this->currenciesRepository->create($data);
    }

    /**
     * @param int $id
     * @param array $data
     * @return Model|Currency
     */
    public function updateById($id, array $data)
    {
        $currency = $this->getById($id);

        return $this->currenciesRepository->update($currency, $data);
    }

    /**
     * @param int $id
     */
    public function deleteById($id)
    {
        $this->currenciesRepository->delete($id);
    }
}
