<?php


namespace Api\Stores\Services;


use Api\Stores\Exceptions\StoreNotFoundException;
use Api\Stores\Models\Store;
use Api\Stores\Repositories\StoresRepository;
use Illuminate\Database\Eloquent\Collection;
use Infrastructure\Database\Eloquent\Model;

/**
 * Class StoresService
 * @package Api\Stores\Services
 */
class StoresService
{
    /**
     * @var StoresRepository
     */
    private $storesRepository;

    /**
     * StoresService constructor.
     * @param StoresRepository $storesRepository
     */
    public function __construct(StoresRepository $storesRepository)
    {
        $this->storesRepository = $storesRepository;
    }

    /**
     * @param array $resourceOptions
     * @return Collection|Store[]
     */
    public function getAll($resourceOptions = [])
    {
        return $this->storesRepository->get($resourceOptions);
    }

    /**
     * @param int $id
     * @return Collection|Store
     */
    public function getById($id)
    {
        $store = $this->storesRepository->getById($id);

        if (!$store) {
            throw new StoreNotFoundException($id);
        }

        return $store;
    }

    /**
     * @param array $data
     * @return Model|Store
     */
    public function create(array $data)
    {
        return $this->storesRepository->create($data);
    }

    /**
     * @param int $id
     * @param array $data
     * @return Model|Store
     */
    public function updateById($id, array $data)
    {
        $store = $this->getById($id);

        return $this->storesRepository->update($store, $data);
    }

    /**
     * @param int $id
     */
    public function deleteById($id)
    {
        $this->storesRepository->delete($id);
    }
}
