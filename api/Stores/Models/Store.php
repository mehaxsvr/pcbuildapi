<?php


namespace Api\Stores\Models;


use Infrastructure\Database\Eloquent\Model;

/**
 * Class Store
 * @package Api\Stores\Models
 * @property string name
 * @property int currency_id
 *
 * @property-read Currency currency
 */
class Store extends Model
{
    protected $table = 'stores';

    protected $fillable = [
        'name', 'currency_id',
    ];

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }
}
