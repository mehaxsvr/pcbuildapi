<?php


namespace Api\Stores\Models;


use Infrastructure\Database\Eloquent\Model;

/**
 * Class Currency
 * @package Api\Stores\Models
 * @property string name
 * @property string short
 * @property float to_eur
 */
class Currency extends Model
{
    protected $table = 'currencies';

    protected $fillable = [
        'name', 'short', 'to_eur',
    ];
}
