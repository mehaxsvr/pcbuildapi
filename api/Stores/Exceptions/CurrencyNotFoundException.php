<?php


namespace Api\Stores\Exceptions;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CurrencyNotFoundException extends NotFoundHttpException
{
    public function __construct($id)
    {
        parent::__construct("Currency not found: $id");
    }
}
