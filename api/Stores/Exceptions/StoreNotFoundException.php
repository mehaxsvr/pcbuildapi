<?php


namespace Api\Stores\Exceptions;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class StoreNotFoundException extends NotFoundHttpException
{
    public function __construct($id)
    {
        parent::__construct("Store not found: $id");
    }
}
