<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('captions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('is_main');
            $table->bigInteger('parent_id')->unsigned()->nullable();
            $table->bigInteger('type_id')->unsigned();
            $table->bigInteger('field_id')->unsigned();
            $table->string('value');
            $table->timestamps();

            $table->foreign('parent_id')->references('id')->on('captions');
            $table->foreign('type_id')->references('id')->on('types');
            $table->foreign('field_id')->references('id')->on('fields');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('captions');
    }
}
