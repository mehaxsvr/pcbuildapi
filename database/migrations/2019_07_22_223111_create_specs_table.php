<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('component_id')->unsigned();
            $table->bigInteger('field_id')->unsigned();
            $table->bigInteger('caption_id')->unsigned();
            $table->string('value');
            $table->timestamps();

            $table->foreign('component_id')->references('id')->on('components');
            $table->foreign('field_id')->references('id')->on('fields');
            $table->foreign('caption_id')->references('id')->on('captions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specs');
    }
}
